# memorygraphs
Ti*k*Z styles to typeset graphs of program memory

This package is on [CTAN][].

Development happens on [GitLab][].
You can download the latest version of the package and the
[latest documentation][doc] there.

## Colophon

Copyright &copy; 2018&ndash;present [Camil Staps][camilstaps].

This work may be distributed and/or modified under the conditions of the LaTeX
Project Public License, either version 1.3 of this license or (at your option)
any later version. The latest version of this license is in
http://www.latex-project.org/lppl.txt and version 1.3 or later is part of all
distributions of LaTeX version 2005/12/01 or later.

This work has the LPPL maintenance status “maintained”.

The Current Maintainer of this work is C. Staps.

This work consists of the files `memorygraphs.sty` and the documentation in
`memorygraphs.tex` and `example-fibonacci.tex`.

[CTAN]: http://www.ctan.org/pkg/memorygraphs
[GitLab]: https://gitlab.com/camilstaps/memorygraphs
[doc]: https://gitlab.com/camilstaps/memorygraphs/-/jobs/artifacts/master/raw/memorygraphs.pdf?job=doc
[camilstaps]: https://camilstaps.nl
