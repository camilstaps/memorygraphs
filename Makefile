TEX:=pdftex
LATEX:=pdflatex
LATEXFLAGS:=-shell-escape
MAKEIDX:=makeindex

PKG:=memorygraphs
DOC:=$(PKG).pdf
STY:=$(PKG).sty
TEX:=$(PKG).tex
TARBALL:=$(PKG).tar.gz

all: $(DOC) $(TARBALL)

$(TARBALL): $(STY) $(TEX) $(DOC) README.md
	tar czv --transform 's!^!$(PKG)/!' -f $@ $^

%.pdf: %.tex %.sty
	$(LATEX) $(LATEXFLAGS) $<
	$(MAKEIDX) $(<:tex=idx)
	$(LATEX) $(LATEXFLAGS) $<
	$(MAKEIDX) $(<:tex=idx)
	$(LATEX) $(LATEXFLAGS) $<

clean:
	$(RM) -v $(addprefix $(PKG).,aux glo gls hd idx ilg ind ins log out tmp toc)

distclean: clean
	$(RM) -v $(addprefix $(PKG),.pdf)

.PHONY: clean distclean
